package resource;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONObject;

import bean.Planeta;
import service.PlanetaService;

@Path("/planetas")
public class PlanetaResource {
	
//	@GET
//	@Produces(MediaType.TEXT_PLAIN)
//	public String sayHelloText() {
//		String resource = "Oi";
//		return resource;
//	}
	
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response listarPlaneta(
			@QueryParam("id") Integer id,
			@QueryParam("nome") String nome,
			@QueryParam("clima") String clima,
			@QueryParam("terreno") String terreno) {
				
		JSONObject obj = PlanetaService.listarPlaneta(id, nome, clima, terreno);
				
		String resultado = "@Produces(\"application/json\") Output: \n\nF to C Converter Output: \n\n" + obj;
		return Response.status(200).entity(resultado).build();
	}
	
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response listarPlaneta(@PathParam("id") Integer id) {
				
		JSONObject obj = PlanetaService.listarPlaneta(id);
		
		String resultado = "@Produces(\"application/json\") Output: \n\nF to C Converter Output: \n\n" + obj;
		return Response.status(200).entity(resultado).build();
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response inserirPlaneta(Planeta planeta) {

		JSONObject obj = PlanetaService.inserirPlaneta(planeta);
		
		String resultado = "@Produces(\"application/json\") Output: \n\nF to C Converter Output: \n\n" + obj;
		return Response.status(200).entity(resultado).build();
			
	}
	
	@DELETE
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response removerPlaneta(@PathParam("id") Integer id) {
		
		JSONObject obj = PlanetaService.removerPlaneta(id);
		
		String resultado = "@Produces(\"application/json\") Output: \n\nF to C Converter Output: \n\n" + obj;
		return Response.status(200).entity(resultado).build();
	}

}
