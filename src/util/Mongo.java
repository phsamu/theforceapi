package util;

import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

import bean.Planeta;

public class Mongo {

	public static void main(String[] args) {
		Integer acao = 1;

		try {
			switch (acao) {
			case 0:
				System.out.println("CRIANDO BANCO DE PLANETAS.");
				if (criarBanco()) {
					System.out.println("BANCO CRIADO.");
				}
				break;
			case 1:
				System.out.println("CRIANDO COLE��O GERAID.");
				if (criaGeraId()) {
					System.out.println("BANCO CRIADO.");
				}
				break;
			default:
				System.out.println("A��O INV�LIDA.");
				break;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private static boolean criarBanco() {
		MongoClient mongoClient = new MongoClient();
		MongoDatabase database = mongoClient.getDatabase("theforce_teste");
		MongoCollection<Document> collection = database.getCollection("planetas");

		Planeta planeta = new Planeta();
		
		planeta.setId(1);
		planeta.setNome("Tatooine");
		planeta.setClima("�rido");
		planeta.setTerreno("Deserto");

		collection.insertOne(planeta.toDocument());

		planeta = new Planeta();

		planeta.setId(2);
		planeta.setNome("Alderaan");
		planeta.setClima("Temperado");
		planeta.setTerreno("Montanha");

		collection.insertOne(planeta.toDocument());

		planeta.setId(3);
		planeta.setNome("Yavin IV");
		planeta.setClima("Temperado");
		planeta.setTerreno("Floresta");

		collection.insertOne(planeta.toDocument());

		mongoClient.close();

		return true;
	}
	
	private static boolean criaGeraId() {
		MongoClient mongoClient = new MongoClient();
		MongoDatabase database = mongoClient.getDatabase("theforce_teste");
		MongoCollection<Document> collection = database.getCollection("geraid");
		
		collection.insertOne(new Document("_id", "planetaId").append("baseId", 0));
		
		return true;
	}

}
