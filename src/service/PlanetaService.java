package service;

import org.bson.Document;
import org.json.JSONArray;
import org.json.JSONObject;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.mongodb.client.FindIterable;
import com.mongodb.util.JSON;

import bean.Planeta;
import dao.PlanetaDAO;
import validation.PlanetaValidation;

public class PlanetaService {
	
	public static JSONObject listarPlaneta(Integer id, String nome, String clima, String terreno) {
		
		Planeta planeta = new Planeta();
		
		if (id != null)
			planeta.setId(id);
		if (nome != null)
			planeta.setNome(nome);
		if (clima != null)
			planeta.setClima(clima.toLowerCase());
		if (terreno != null)
			planeta.setTerreno(terreno.toLowerCase());
		
		FindIterable<Document> lista = PlanetaDAO.listarPlaneta(planeta);
		
		if (lista == null) {
			return null;
		}
		
		JSONArray data = new JSONArray();
		
		for (Document item : lista) {
			JSONObject jsonItem = new JSONObject()
					.put("id", item.getInteger("id"))
					.put("nome", item.getString("nome"))
					.put("clima", item.getString("clima"))
					.put("terreno", item.getString("terreno"))
					.put("filmes", PlanetaService.listarFilme(item.getString("nome")));
			data.put(jsonItem);
		}
		
		JSONObject obj = new JSONObject().put("data", data);
		
		return obj;
	}
	
	public static JSONObject listarPlaneta(Integer id) {
		return listarPlaneta(id, null, null, null);
	}
	
	public static JSONObject inserirPlaneta(Planeta planeta) {
		
		String validacao = PlanetaValidation.validarInclusao(planeta);
		
		if (validacao != null) {
			JSONObject obj = new JSONObject().put("data", new JSONArray());
			return obj.put("erro", validacao);
		}
		
		Integer id = PlanetaDAO.geraid(); 
		
		if (id == null) {
			JSONObject obj = new JSONObject().put("data", new JSONArray());
			return obj.put("erro", "N�o foi poss�vel inserir planeta (erro geraId).");
		}
		
		planeta.setId(id);
		//planeta.setNome(formataTextoUpper(planeta.getNome()));
		planeta.setClima(planeta.getClima().toLowerCase());
		planeta.setTerreno(planeta.getTerreno().toLowerCase());
		
		if (!PlanetaDAO.inserirPlaneta(planeta)) {
			JSONObject obj = new JSONObject().put("data", new JSONArray());
			return obj.put("erro", "N�o foi poss�vel inserir planeta (erro inserirPlaneta).");
		}
		
		return listarPlaneta(planeta.getId()).put("message", "Planeta inserido com sucesso.");
	}
	
	public static JSONObject removerPlaneta(Integer id) {
		
		Planeta planetaRemover = new Planeta();
		planetaRemover.setId(id);
		
		String validacao = PlanetaValidation.validarRemocao(planetaRemover);
		
		if (validacao != null) {
			JSONObject obj = new JSONObject().put("data", new JSONArray());
			return obj.put("erro", validacao);
		}
		
		if (!PlanetaDAO.removerPlaneta(id)) {
			JSONObject obj = new JSONObject().put("data", new JSONArray());
			return obj.put("erro", "N�o foi poss�vel remover o planeta (erro removerPlaneta).");
		}
		
		return new JSONObject().put("data", new JSONArray()).put("message", "Planeta removido com sucesso.");
	}
	
	private static JSONArray listarFilme(String nome) {
		if (nome == null || nome.isEmpty())
			return new JSONArray();
		
		JSONArray resultado = new JSONArray();
		
		try {
			HttpResponse<JsonNode> jsonResponse = Unirest.get("https://swapi.co/api/planets?search=" + nome.replace(" ", "+"))
					.header("accept", "application/json")
					.asJson();
			
			JSONObject objResponse = jsonResponse.getBody().getObject();
			if (objResponse.getJSONArray("results").length() == 0)
				return resultado;
			JSONArray filmes = objResponse.getJSONArray("results").getJSONObject(0).getJSONArray("films");
			
			for (int i = 0; i < filmes.length(); i++) {
				HttpResponse<JsonNode> jsonFilmeResponse = Unirest.get(filmes.get(i).toString())
						.header("accept", "application/json")
						.asJson();
				
				JSONObject objFilme = jsonFilmeResponse.getBody().getObject();
				resultado.put(objFilme.getString("title"));
			}
			
			return resultado;
		} catch (UnirestException e) {
			e.printStackTrace();
			return new JSONArray();
		}
	}
	
}
