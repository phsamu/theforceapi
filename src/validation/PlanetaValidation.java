package validation;

import bean.Planeta;
import service.PlanetaService;

public class PlanetaValidation {
	
	private static String msgNulo = "N�o � poss�vel inserir nulo.";
	private static String msgNome = "N�o � poss�vel inserir o campo de nome vazio.";
	private static String msgClima = "N�o � poss�vel inserir o campo de clima vazio.";
	private static String msgTerreno = "N�o � poss�vel inserir o campo de terreno vazio.";
	private static String msgExistente = "Um planeta com este nome j� foi inserido.";
	private static String msgId = "ID nulo.";
	private static String msgInexistente = "N�o foi encontrado nenhum planeta com o ID informado.";

	public static String validarInclusao(Planeta planeta) {
		if (planeta == null) {
			return msgNulo;
		}
		if (planeta.getNome() == null || planeta.getNome().isEmpty()) {
			return msgNome;
		}
		if (planeta.getClima() == null || planeta.getClima().isEmpty()) {
			return msgClima;
		}
		if (planeta.getTerreno() == null || planeta.getTerreno().isEmpty()) {
			return msgTerreno;
		}
		Planeta filtro = new Planeta();
		filtro.setNome(planeta.getNome());
		if (PlanetaService.listarPlaneta(null, filtro.getNome(), null, null).getJSONArray("data").length() > 0) {
			return msgExistente;
		}
		return null;
	}
	
	public static String validarRemocao(Planeta planeta) {
		if (planeta == null) {
			return msgNulo;
		}
		if (planeta.getId() == null) {
			return msgId;
		}
		if (PlanetaService.listarPlaneta(planeta.getId()).getJSONArray("data").length() == 0) {
			return msgInexistente;
		}
		return null;
	}
}
