package dao;

import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.MongoIterable;
import com.mongodb.client.model.Collation;
import com.mongodb.client.model.CollationStrength;
import com.mongodb.client.result.DeleteResult;

import bean.Planeta;

public class PlanetaDAO {
	
	private static String NOMEDATABASE = "theforce";
	private static String NOMECOLLECTION = "planetas";
	private static String NOMEGERAID = "geraid";
	private static String HOST = "localhost";
	private static Integer PORT = 27017;

	public static boolean inserirPlaneta(Planeta planeta) {
		try {
			verificaBanco();			
			MongoClient mongoClient = new MongoClient(HOST, PORT);
			MongoDatabase database = mongoClient.getDatabase(NOMEDATABASE);
			MongoCollection<Document> collection = database.getCollection(NOMECOLLECTION);

			collection.insertOne(planeta.toDocument());
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public static FindIterable<Document> listarPlaneta(Planeta planeta) {
		verificaBanco();
		MongoClient mongoClient = new MongoClient(HOST, PORT);
		MongoDatabase database = mongoClient.getDatabase(NOMEDATABASE);
		MongoCollection<Document> collection = database.getCollection(NOMECOLLECTION);
		
		if (planeta == null) {
			return collection.find();
		}
		
		Document filtro = new Document();
		
		if (planeta.getId() != null)
			filtro.put("id", planeta.getId());
		
		if (planeta.getNome() != null && !planeta.getNome().isEmpty())
				filtro.put("nome", planeta.getNome());
		
		if (planeta.getClima() != null && !planeta.getClima().isEmpty()) {
			Document regex = new Document().append("$regex", ".*" + planeta.getClima() + ".*");
			filtro.put("clima", regex);
		}
		
		if (planeta.getTerreno() != null && !planeta.getTerreno().isEmpty()) {
			Document regex = new Document().append("$regex", ".*" + planeta.getTerreno() + ".*");
			filtro.put("terreno", regex);
		}
		
		return collection.find(filtro).collation(Collation.builder()
				.locale("en")
				.collationStrength(CollationStrength.SECONDARY)
				.build());
	}
	
	public static boolean removerPlaneta(Integer id) {
		verificaBanco();
		try {
			MongoClient mongoClient = new MongoClient(HOST, PORT);
			MongoDatabase database = mongoClient.getDatabase(NOMEDATABASE);
			MongoCollection<Document> collection = database.getCollection(NOMECOLLECTION);

			DeleteResult resultado = collection.deleteOne(new Document("id", id));

			if (resultado.getDeletedCount() > 0)
				return true;
			
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return false;
	}
	
	public static Integer geraid() {
		verificaBanco();
		MongoClient mongoClient = new MongoClient(HOST, PORT);
		MongoDatabase database = mongoClient.getDatabase(NOMEDATABASE);
		MongoCollection<Document> collection = database.getCollection(NOMEGERAID);
		
		collection.findOneAndUpdate(new Document("_id", "planetaId"),new Document("$inc",new Document("baseId", 1)));
		FindIterable<Document> id = collection.find(new Document("_id", "planetaId"));
		
		return id.first().getInteger("baseId");
	}
	
	private static void verificaBanco() {
		boolean geraId = false;
		boolean planetas = false;
		boolean theForce = false;
		
		MongoClient mongoClient = new MongoClient(HOST, PORT);
		
		MongoIterable<String> databases = mongoClient.listDatabaseNames();
		
		for (String database : databases) {
			if (database.equalsIgnoreCase(NOMEDATABASE))
				theForce = true;
		}
		
		if (!theForce)
			mongoClient.getDatabase(NOMEDATABASE);
		
		MongoDatabase databaseTheForce = mongoClient.getDatabase(NOMEDATABASE);
		
		MongoIterable<String> collections = databaseTheForce.listCollectionNames();
		
		for (String collection : collections) {
			if (collection.equalsIgnoreCase(NOMECOLLECTION)) {
				planetas = true;
			} else if (collection.equalsIgnoreCase(NOMEGERAID)) {
				geraId = true;
			}
		}
		
		if (!planetas)
			databaseTheForce.getCollection(NOMECOLLECTION);
		
		if (!geraId) {
			MongoCollection<Document> collectionGeraId = databaseTheForce.getCollection(NOMEGERAID);
			collectionGeraId.insertOne(new Document("_id", "planetaId").append("baseId", 0));
		}
	}

}
