# The Force API

API baseada em REST e desenvolvida em Java para acessar dados referentes aos planetas da saga STAR WARS

## Desenvolvido com

* [Apache Tomcat 8.5](https://tomcat.apache.org/download-80.cgi)
* [MongoDB 4.0](https://www.mongodb.com/download-center#community)
* [SWAPI](https://swapi.co/)
* [Jersey JAX-RS 2.1](https://jersey.github.io/download.html)
* [MongoDB Java Driver 3.8.2](https://mongodb.github.io/mongo-java-driver/)
* [Unirest 1.4.9](http://unirest.io/java.html)

## Funcionalidades

* Adicionar um planeta (com nome, clima e terreno)
* Listar planetas
* Buscar por nome
* Buscar por ID
* Remover planeta

## Testes

Os testes foram realizados com auxílio da ferramenta [Postman](https://www.getpostman.com/) e podem ser encontrados [aqui](https://web.postman.co/collections/5487694-66f7d8f2-1db0-4959-a0a6-318a556e40ec?workspace=1d3828ef-fdfe-4b0e-827b-b76c475d6afc)

## Casos de teste

### Lista de planetas - GET

```
http://localhost:8080/TheForceAPI/planetas/
```

### Busca por nome - GET

```
http://localhost:8080/TheForceAPI/planetas?nome=Tatooine
```

### Busca por clima - GET

```
http://localhost:8080/TheForceAPI/planetas?clima=temperado
```

### Busca por terreno - GET

```
http://localhost:8080/TheForceAPI/planetas?clima=montanhoso
```

### Busca por ID - GET

```
http://localhost:8080/TheForceAPI/planetas?id=1
http://localhost:8080/TheForceAPI/planetas/1
```

### Inserir planeta - POST

```
http://localhost:8080/TheForceAPI/planetas/
```

```
{
	"nome": "Yavin IV",
    "clima": "temperado, tropical",
    "terreno": "selva, floresta tropical"
}
```

### Remover planeta - DELETE

```
http://localhost:8080/TheForceAPI/planetas/1
```

## Autor

Pedro Henrique Gueiros Samú